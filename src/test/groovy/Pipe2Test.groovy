import com.lesfurets.jenkins.unit.BasePipelineTest
import org.junit.Before
import org.junit.Test

/**
 * @author presnuhin-aa on 09.03.2021
 */
class Pipe2Test extends BasePipelineTest {
    @Override
    @Before
    void setUp() throws Exception {
        scriptExtension = 'groovy'
        super.setUp()
        helper.registerAllowedMethod("timestamps", [Closure.class], { println 'Printing timestamp' })
    }

    @Test
    void a_should_execute_without_errors() throws Exception {
        runScript("pipe2.groovy")
    }

    @Test
    void method_execute_without_errors() throws Exception {
        def script = loadScript("pipe2.groovy")
        script.somePrint('456')
        printCallStack()
    }

    @Test
    void loadScript() throws Exception {
        def script = loadScript("pipe2.groovy")
    }
}
