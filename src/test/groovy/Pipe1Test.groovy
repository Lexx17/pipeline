import com.lesfurets.jenkins.unit.BasePipelineTest
import org.junit.Before
import org.junit.Test

class Pipe1Test extends BasePipelineTest {
    @Override
    @Before
    void setUp() throws Exception {
        scriptExtension = 'groovy'
        super.setUp()
    }

    @Test
    void should_execute_without_errors() throws Exception {
        def script = loadScript("pipe1.groovy")
        script.execute()
        printCallStack()
    }
}
