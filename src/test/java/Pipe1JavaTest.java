import com.lesfurets.jenkins.unit.BasePipelineTest;
import groovy.lang.Script;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;

/**
 * @author presnuhin-aa on 09.03.2021
 */
@RunWith(BlockJUnit4ClassRunner.class)
public class Pipe1JavaTest extends BasePipelineTest {
    @Override
    @Before
    public void setUp() throws Exception {
        setScriptExtension("groovy");
        super.setUp();
    }

    @Test
    public void should_execute_without_errors() {
        Script script = loadScript("pipe1.groovy");
        script.invokeMethod("execute", null);
        printCallStack();
    }
}
